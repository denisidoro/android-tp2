package android.ecp.fr.todolist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;
import java.util.zip.Inflater;

public class TodoActivity extends ListActivity implements FormDialogFragment.FormDialogListener {

    private ListAdapter todoListAdapter;
    private TodoListSQLHelper todoListSQLHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        updateTodoList();
        lineThroughDoneTasks();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_todo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add_task:
                DialogFragment newFragment = new FormDialogFragment();
                newFragment.show(getFragmentManager(), "form");
                return true;

            case R.id.action_settings:
                AlertDialog.Builder builder = new AlertDialog.Builder(TodoActivity.this);
                builder.setMessage("Check OK if you want to store your data online")
                        .setTitle("Use Parse?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;

            case R.id.action_about:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(TodoActivity.this);
                builder2.setMessage("Leonardo Dantas\nDenis Isidoro")
                        .setTitle("Creators");
                AlertDialog dialog2 = builder2.create();
                dialog2.show();
                return true;

            default:
                return false;

        }
    }

    //update the todo task list UI
    private void updateTodoList() {
        todoListSQLHelper = new TodoListSQLHelper(TodoActivity.this);
        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getReadableDatabase();

        //cursor to read todo task list from database
        Cursor cursor = sqLiteDatabase.query(TodoListSQLHelper.TABLE_NAME,
                new String[]{TodoListSQLHelper._ID, TodoListSQLHelper.COL_NAME, TodoListSQLHelper.COL_COMMENT, TodoListSQLHelper.COL_DATE,TodoListSQLHelper.COL_PERIOD, TodoListSQLHelper.COL_LABEL},
                null, null, null, null, null);

        //binds the todo task list with the UI
        todoListAdapter = new SimpleCursorAdapter(
                this,
                R.layout.todotask,
                cursor,
                new String[]{TodoListSQLHelper.COL_NAME, TodoListSQLHelper.COL_COMMENT, TodoListSQLHelper.COL_DATE, TodoListSQLHelper.COL_PERIOD, TodoListSQLHelper.COL_LABEL},
                new int[]{R.id.todoNameTV, R.id.todoCommentTV, R.id.todoDateTV, R.id.todoPeriodTV, R.id.todoLabelTV},
                0
        );

        this.setListAdapter(todoListAdapter);
    }

    //closing the todo task item
    public void onDoneButtonClick(View view) {
        View v = (View) view.getParent();
        TextView todoTV = (TextView) v.findViewById(R.id.todoNameTV);
        String todoTaskItem = todoTV.getText().toString();

        String deleteTodoItemSql = "DELETE FROM " + TodoListSQLHelper.TABLE_NAME +
                " WHERE " + TodoListSQLHelper.COL_NAME + " = '" + todoTaskItem + "'";

        todoListSQLHelper = new TodoListSQLHelper(TodoActivity.this);
        SQLiteDatabase sqlDB = todoListSQLHelper.getWritableDatabase();
        sqlDB.execSQL(deleteTodoItemSql);
        updateTodoList();
        lineThroughDoneTasks();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        String todoNameInput = ((EditText) dialog.getDialog().findViewById(R.id.formName)).getText().toString();
        String todoCommentInput = ((EditText) dialog.getDialog().findViewById(R.id.formComment)).getText().toString();
        DatePicker dp = (DatePicker) dialog.getDialog().findViewById(R.id.formDate);

        String todoDateInput = dp.getDayOfMonth() + "/" + dp.getMonth() + "/" + dp.getYear();

        String todoPeriodInput = ((Spinner) dialog.getDialog().findViewById((R.id.formPeriod))).getSelectedItem().toString();
        String todoLabelInput = ((Spinner) dialog.getDialog().findViewById((R.id.formLabel))).getSelectedItem().toString();

        todoListSQLHelper = new TodoListSQLHelper(TodoActivity.this);
        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.clear();

        //write the Todo task input into database table
        values.put(TodoListSQLHelper.COL_NAME, todoNameInput);
        values.put(TodoListSQLHelper.COL_COMMENT, todoCommentInput);
        values.put(TodoListSQLHelper.COL_DATE, todoDateInput);
        values.put(TodoListSQLHelper.COL_PERIOD, todoPeriodInput);
        values.put(TodoListSQLHelper.COL_LABEL, todoLabelInput);

        sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);

        //update the Todo task list UI
        updateTodoList();

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
    }

    // ESTA FUNCAO NAO FAZ NADA AINDA!!!
    public void lineThroughDoneTasks() {

        ListView lv = getListView();

        for (int i = 0; i < lv.getCount(); i++) {
            View v = lv.getAdapter().getView(i, null, null);
            TextView tx = (TextView) v.findViewById(R.id.todoNameTV);
            Log.d("list_element", Integer.toString(i) + ": " + tx.getText().toString());
            tx.setTextColor(Color.RED);
            tx.setText("THE_ADAPTER_MUST_BE_EXTENDED_FOR_THIS_TO_WORK");
        }

    }


}
