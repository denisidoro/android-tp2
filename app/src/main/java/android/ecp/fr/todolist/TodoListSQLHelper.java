package android.ecp.fr.todolist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class TodoListSQLHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "fr.ecp.android.todolist";
    public static final String TABLE_NAME = "TODO_LIST";
    public static final String COL_NAME = "name";
    public static final String COL_COMMENT = "comment";
    public static final String COL_DATE = "date";
    public static final String COL_PERIOD = "period";
    public static final String COL_LABEL = "label";
    public static final String _ID = BaseColumns._ID;

    public TodoListSQLHelper(Context context) {
        //1 is todo list database version
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqlDB) {
        String createTodoListTable = "CREATE TABLE " + TABLE_NAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_NAME + " TEXT, " + COL_PERIOD + " TEXT, " + COL_LABEL + " TEXT, " + COL_DATE + " TEXT, " + COL_COMMENT + " TEXT)";
        sqlDB.execSQL(createTodoListTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqlDB, int i, int i2) {
        sqlDB.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqlDB);
    }
}